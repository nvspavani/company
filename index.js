var express = require('express');
var bodyParser = require('body-parser');
var mysql =require('mysql');
var app = express();
var Sequelize = require('sequelize');

const sequelize = new Sequelize('company', 'root', 'admin', {
    host: 'localhost',
    dialect: 'mysql',

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
 });
const Company = sequelize.define('company', {
    companyName: {
        type: Sequelize.STRING
    }


});
const Employee = sequelize.define('employee', {
    employeeName: {
        type: Sequelize.STRING
    },
    employeeMobileNumber:{
        type: Sequelize.STRING
    }

});


var connection=mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'admin',
    database : 'company'
});
connection.connect();
app.set('port', (process.env.PORT || 2050));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.post('/EmployeeDetails', function(req, res) {
    Employee.create({
        employeeName: req.body.employeeName,
        companyName: req.body.companyName,
        employeeMobileNumber: req.body.employeeMobileNumber,
        companyId: req.body.companyId
    })
            res.send("sending data...");

    });

app.post('/CompanyDetails', function(req, res) {
    Company.create({
        companyName: req.body.companyName
    }).then(companyDetails => {
            Company.findAll({
             include: [{
                 model: Employee
             }]
        }).then(companies => {
            res.send(companies);
        });
    });

});

Company.hasMany(Employee,{foreignkey : 'companyId'});

app.listen(app.get('port'), function() {
    console.log('Server started..: http://localhost:' + app.get('port') + '/');
});


sequelize.sync({

});